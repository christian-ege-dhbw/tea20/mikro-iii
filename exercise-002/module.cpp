#include "data.h"
#include <fmt/format.h>

static int data = 256;


static void print_helper(void) {
        fmt::print("[print_helper] value of data {} address of pointer {}\n",data,fmt::ptr(&data));
}

void print_data(void) {
    print_helper();
}