#include <fmt/format.h>
#include <fmt/chrono.h>
#include "config.h"
#include "data.h"
#include "data.h"

#include <vector>

/* RO Data Segment */
const int foo = 100;

/* BSS Segment */
int bar;
int bar_ = 0;

/* Data Segment */
int blub{42};
int baz{24};

//std::vector<int&> data_store;

void myFunction(void) {
  static int counter{0};
  int value{42};
  fmt::print("[myFunction] value of counter {} address of counter {}\n",counter,fmt::ptr(&counter));
  fmt::print("[myFunction] value of value {} address of value {}\n",value,fmt::ptr(&value));
  counter++;
  if(counter >= 5) {
    return;
  }
  myFunction();
}

// void print_helper();

int my_function2(int* a, int* b)
{
  int zoo = 25;
  /*a = nullptr;*/
  a = &zoo;
  fmt::print("[my_function2] Value of a:    {} address of a     {}\n",*a, fmt::ptr(a));
  fmt::print("[my_function2] Value of b:    {} address of b     {}\n",*b , fmt::ptr(b));
  *a = *a + 1;
  *b = *b + 1;
  return *a + *b;
}

int my_function3(int& a, int& b)
{
  fmt::print("[my_function3] Value of a:    {} address of a     {}\n",a, fmt::ptr(&a));
  fmt::print("[my_function3] Value of b:    {} address of b     {}\n",b , fmt::ptr(&b));
  a = a + 1;
  b = b + 1;
  return a + b;
}

int main(int argc, char **argv)
{

  static int local;
  static int local1{34};

  /**
   * The {fmt} lib is a cross platform library for printing and formatting text
   * it is much more convenient than std::cout and printf
   * More info at https://fmt.dev/latest/api.html
   */
  fmt::print("Hello, {}!\n", "World");


  fmt::print("Value of foo:    {} address of foo     {}\n", foo, fmt::ptr(&foo));
  fmt::print("Value of bar:    {} address of bar     {}\n", bar, fmt::ptr(&bar));
  fmt::print("Value of bar_:   {} address of bar_    {}\n", bar_, fmt::ptr(&bar_));
  fmt::print("Value of blub:   {} address of blub    {}\n", blub, fmt::ptr(&blub));
  fmt::print("Value of baz:    {} address of baz     {}\n", baz, fmt::ptr(&baz));


  fmt::print("Value of local:    {} address of local     {}\n", local, fmt::ptr(&local));
  fmt::print("Value of local1:    {} address of local1     {}\n", local1, fmt::ptr(&local1));

  myFunction();
  fmt::print("second call\n");
  myFunction();

  /* INSERT YOUR CODE HERE */

  //fmt::print("Value of data:    {} address of data     {}\n", data, fmt::ptr(&data));
  print_data();
  //print_helper();

  fmt::print("Value of bar_:    {} address of bar_     {}\n",bar_, fmt::ptr(&bar_));
  fmt::print("Value of bar:    {} address of bar     {}\n",bar , fmt::ptr(&bar));
  my_function2(&bar_,&bar);
  fmt::print("Value of bar_:    {} address of bar_     {}\n",bar_, fmt::ptr(&bar_));
  fmt::print("Value of bar:    {} address of bar     {}\n",bar , fmt::ptr(&bar));


  fmt::print("Value of bar_:    {} address of bar_     {}\n",bar_, fmt::ptr(&bar_));
  fmt::print("Value of bar:    {} address of bar     {}\n",bar , fmt::ptr(&bar));
  my_function3(bar_,bar);
  fmt::print("Value of bar_:    {} address of bar_     {}\n",bar_, fmt::ptr(&bar_));
  fmt::print("Value of bar:    {} address of bar     {}\n",bar , fmt::ptr(&bar));


  return 0; /* exit gracefully*/
}
