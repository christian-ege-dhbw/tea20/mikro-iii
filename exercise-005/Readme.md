# Statemachine

This is just a simple project to demonstrate various implementation techniques for a state machine. There are solution for the programming language C only at the moment

## Statemachine Diagram

```mermaid
stateDiagram-v2
    [*] --> FAHRE_HOCH
    OBEN --> FAHRE_RUNTER : Taste
    OBEN --> OBEN : Enschalter Oben
    FAHRE_HOCH --> OBEN : Enschalter Oben
    FAHRE_HOCH --> FAHRE_RUNTER: Taste
    FAHRE_RUNTER --> UNTEN : Enschalter Unten
    FAHRE_RUNTER --> FAHRE_HOCH : Taste 
    UNTEN --> FAHRE_HOCH : Taste
    UNTEN --> UNTEN : Enschalter Unten
    OBEN --> ERROR : Enschalter Unten
    UNTEN --> ERROR : Enschalter Oben
    FAHRE_RUNTER --> ERROR: Endschalter Oben
    FAHRE_HOCH --> ERROR: Endschalter Unten
```

# Prerequisites

- CMake (>= 2.8.12)
- GCC (>= 4.8)

LICENSE

Please see the file called [LICENSE](LICENSE). 
