#include <fmt/format.h>
#include <fmt/chrono.h>


int main(int argc, char **argv)
{


  /**
   * The {fmt} lib is a cross platform library for printing and formatting text
   * it is much more convenient than std::cout and printf
   * More info at https://fmt.dev/latest/api.html
   */
  fmt::print("Hello, {}!\n", "World");


  /* INSERT YOUR CODE HERE */

  return 0; /* exit gracefully*/
}
