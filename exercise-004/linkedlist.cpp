#include "linkedlist.hpp"

LinkedList::~LinkedList()
{
  for (auto tmp = m_head; tmp != nullptr; tmp = tmp->pNext /* prepare the next in the row */) {
    auto elem = tmp; /* save the element to be deleted */
    delete elem;
  }
}

bool LinkedList::insert_tail(LinkedListNode *node)
{
  bool ret = false;
  if (nullptr == node) {
    return ret;
  }
  if (nullptr == m_head) {
    /* empty list*/
    m_head = node;
    m_tail = node;
  } else {
    return insert_after(m_tail, node);
  }
  return ret;
}

bool LinkedList::insert_head(LinkedListNode *node)
{
  bool ret = false;
  if (nullptr == node) {
    return ret;
  }
  if (nullptr == m_head) {
    /* empty list*/
    m_head = node;
    m_tail = node;
  } else {
    ret = insert_before(m_head, node);
  }
  return ret;
}

bool LinkedList::insert_after(LinkedListNode *loc, LinkedListNode *node)
{
  bool ret = false;
  if ((nullptr == loc) || (nullptr == node)) {
    return ret;
  }
  if (nullptr == loc->pNext) {
    /*End of the list*/
    m_tail = node;
  }
  loc->pNext = node;
  ret = true;
  return ret;
}

bool LinkedList::insert_before(LinkedListNode *loc, LinkedListNode *node)
{
  bool ret = false;
  if ((nullptr == loc) || (nullptr == node)) {
    return ret;
  }
  if (m_head == loc) {
    node->pNext = m_head;
    m_head = node;
    ret = true;
  } else {
    auto tmp = m_head;
    while (tmp) {
      if (tmp->pNext == loc) {
        break;
      }
      tmp = tmp->pNext;
    }
    if (tmp->pNext == loc) {
      node->pNext = loc;
      tmp->pNext = loc;
      ret = true;
    } else {
      ret = false;
    }
  }
  return ret;
}

bool LinkedList::remove(LinkedListNode *node)
{
  bool ret = false;
  auto tmp = m_head;
  while (tmp) {
    if (tmp->pNext == node) {
      break;
    }
    tmp = tmp->pNext;
  }
  if (tmp->pNext == node) {
    tmp->pNext = tmp->pNext->pNext;
    ret = true;
  } else {
    /* Element not found */
    ret = false;
  }
  return ret;
}

size_t LinkedList::size()
{
  size_t count = 0;
  /* using a lambda to count objects in the list*/
  traverse([&count](LinkedListNode *node) { count++; });
  return count;
}


void LinkedList::traverse(std::function<void(const std::string &)> func)
{
  traverse([&](LinkedListNode *node) { func(node->m_name); });
}

void LinkedList::traverse(std::function<void(LinkedListNode *node)> func)
{
  for (auto tmp = m_head; tmp != nullptr; tmp = tmp->pNext) {
    func(tmp);
  }
}