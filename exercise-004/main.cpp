#include <fmt/format.h>
#include <fmt/chrono.h>
#include <CLI11.hpp>

#include "config.h"
#include "linkedlist.hpp"


void printer(const std::string &name)
{
  fmt::print("Printer: {}\n", name);
}


int main(int argc, char **argv)
{
  /**
   * CLI11 is a command line parser to add command line options
   * More info at https://github.com/CLIUtils/CLI11#usage
   */
  CLI::App app{ PROJECT_NAME };
  try {
    app.set_version_flag("-V,--version", fmt::format("{} {}", PROJECT_VER, PROJECT_BUILD_DATE));
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  /**
   * The {fmt} lib is a cross platform library for printing and formatting text
   * it is much more convenient than std::cout and printf
   * More info at https://fmt.dev/latest/api.html
   */
  fmt::print("Hello, Linked List: {}!\n", app.get_name());

  const std::vector<std::string> names{ "Hugo", "Franz", "Elisabeth", "Anton", "Gerhard", "Maria", "Hannelore" };
  const std::vector<std::string> names2{ "Ulf", "Ole", "Sepp", "Norton", "Kai", "Uschi", "Gert" };
  {
    LinkedList list;
    fmt::print("------------------------------\n");
    for (auto &name : names) {
      auto elem = new LinkedListNode(name);
      // elem->print();
      list.insert_tail(elem);
    }
    list.traverse(printer);
    fmt::print("------------------------------\n");
    for (auto &name : names2) {
      auto elem = new LinkedListNode(name);
      // elem->print();
      list.insert_head(elem);
    }
    fmt::print("------------------------------\n");
    list.traverse(printer);
    fmt::print("------------------------------\n");
    fmt::print("Elements in the list: {}\n", list.size());
    fmt::print("------------------------------\n");
    list.traverse(printer);
  } /* this calls the DTOR of the list*/

  return 0; /* exit gracefully*/
}
